#!/usr/bin/python3

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import requests
import time
import concurrent.futures
from lxml import etree


from selenium.webdriver.chrome.options import Options
chrome_options = Options()
#chrome_options.add_argument("--disable-extensions")
#chrome_options.add_argument("--disable-gpu")
#chrome_options.add_argument("--no-sandbox") # linux only
chrome_options.add_argument("--headless")

DRIVER_PATH = '/home/jc/forge/chromedriver'
driver = webdriver.Chrome(executable_path=DRIVER_PATH, options=chrome_options)
driver.get('https://www.ariva.de/zertifikate/suche/index.m?zertart_id=13#eq_underlying_ag=5873')
# <button tabindex="0" title="Zustimmen" aria-label="Zustimmen" class="message-component message-button no-children" path="[0,1,1,0]" style="padding: 10px 15px; margin: 5px 10px; border-width: 1px; border-color: rgb(0, 56, 93); border-radius: 4px; border-style: solid; font-size: 14px; font-weight: 400; color: rgb(255, 255, 255); font-family: verdana, geneva, sans-serif; width: 350px; background: rgb(0, 56, 93);">Zustimmen</button>
print("loaded.")
driver.switch_to_frame(driver.find_element_by_id("sp_message_iframe_213940"))
driver.find_element_by_xpath('//*[@title="Zustimmen"]').click()
driver.switch_to_default_content()
WebDriverWait(driver, timeout=100).until(lambda d : d.find_element_by_id("PAGINATION_BLOCK"))
# driver.find_element_by_link_text("Spalten ändern").click()
# select = webdriver.support.ui.Select(driver.find_elements_by_class_name("select_title")[7])
# select.select_by_value("ratio")
# driver.find_element_by_link_text("Änderungen übernehmen").click()
search_id = driver.current_url.split("search_id=")[1].split("&")[0]
print("SEARCH ID:", search_id)
requestline = "https://www.ariva.de/zertifikate/suche/_result_table_content?search_id=placeholder&shown_cols_csv=wkn%2Cemittent_kurzname%2Claufzeit_ende%2Cstrike%2Ck_omega%2Ck_spread_rel%2Ck_implied_vola%2Cratio%2Ckurs_bid%2Ckurs_ask%2Ctyp&current_page=2&results_per_page=10&calling_campaign_id=0&calling_emittent_id=0&sortBy=k_omega&sortDir=desc&ism_typ=zertifikate&tool=Derivatesuche"
requestline = requestline.split("search_id=")[0]+"search_id="+search_id+"".join("&"+x for x in requestline.split("search_id=")[1].split("&")[1:])
# print(requestline)
# waitblock = driver.find_element_by_id("zv_page_waiting")
# WebDriverWait(driver, timeout=100).until(lambda _ : not waitblock.is_displayed())
# print("clear")
# table = driver.find_element_by_id("RESULT_TABLE_CONTENT_BLOCK")
# WKNs_all = list()
# WKNs={x.split()[0]:1 for x in table.text.split('\n')}
# WKNs_all+=[x.split()[0] for x in table.text.split('\n')]
# print(len(WKNs)%30)
# print(len(WKNs_all)%30)
# for page in range(2, pages+1):
#     # driver.find_element_by_link_text("Weiter").click()
#     # WebDriverWait(driver, timeout=100, poll_frequency=0.05).until(lambda _ : not waitblock.is_displayed())
#     # print("clear", page)
#     # for x in table.text.split('\n'):
#     #     w = x.split()[0]
#     #     if x.split()[0] in WKNs:
#     #         print(w, WKNs[w])
#     #     else:
#     #         WKNs[w] = page
#     # # WKNs.update(x.split()[0] for x in table.text.split('\n'))
#     # WKNs_all+=[x.split()[0] for x in table.text.split('\n')]
#     # print(len(WKNs)%30)
#     # print(len(WKNs_all)%30)
#     requestline = requestline.split("current_page=")[0]+"current_page="+str(page)+"".join("&"+x for x in requestline.split("current_page=")[1].split("&")[1:])
#     # print(requestline)
#     driver = selreq.Chrome(executable_path=DRIVER_PATH)
#     response = driver.request("GET", requestline)
#     mocksite = "<html><body><table>"+response.text+"</table></body></html>"
#     driver.get("data:text/html;charset=utf-8,"+mocksite)
#     print("".join(x.text+"\n" for x in driver.find_elements_by_class_name("quickinfo_trigger")))
#     # tabletext += driver.find_element_by_tag_name("body").text

def job(requestline, page):
    # print(page, "requesting")
    req = requestline.split("current_page=")[0]+"current_page="+str(page)+"".join("&"+x for x in requestline.split("current_page=")[1].split("&")[1:])
    # print(req)
    response = requests.get(req).text
    # print(response)
    if (not response):
        print(page, "NO RESPONSE!!!!!!")
    return response

pages = int(driver.find_element_by_id("PAGINATION_BLOCK").find_element_by_tag_name("h3").text.split("(")[-1].split(")")[0].replace(".",""))//30
print(pages, "pages expected.")
import sys
with concurrent.futures.ThreadPoolExecutor(max_workers=1024) as executor:
    futures = set(executor.submit(job, requestline, page) for page in range(1, pages+1))
    responses = "".join(x.result() for x in concurrent.futures.as_completed(futures))
print("requests done.")


# WKNs = set()
res = "".join(responses)
mocksite = "<html><body><table>"+res+"</table></body></html>"
table = etree.HTML(mocksite).find("body/table")
WKNs = set(x[1][0].text for x in table if len(x)==12)
print(len(WKNs))
driver.quit()

# print(len(WKNs), len(WKNs)/pages)
# print(len(WKNs_all), len(WKNs_all)/pages)


